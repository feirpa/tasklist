package br.com.feirpa.tasklist;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button btnInserir;
    private ListView lstLista;
    private TextView txtTexto;

    private SQLiteDatabase bd;

    private ArrayAdapter<String> itensAdapter;
    private ArrayList<Integer> ids;
    private ArrayList<String> itens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtTexto = (TextView) findViewById(R.id.txtTexto);
        btnInserir = (Button) findViewById(R.id.btnInserir);
        lstLista = (ListView) findViewById(R.id.lstLista);

        carregaTarefas();

        btnInserir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adicionarNovaTarefa(txtTexto.getText().toString());
            }
        });

        lstLista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                //apagarTarefa(ids.get(position));
                alertaApagar(position);
                return false;
            }
        });
    }

    private void carregaTarefas() {
        try {
            bd = openOrCreateDatabase("ToDoList", MODE_PRIVATE, null);
            bd.execSQL("CREATE TABLE IF NOT EXISTS minhasTarefas"
                    + "(id INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + "tarefa VARCHAR)");

            Cursor cursor = bd.rawQuery("SELECT * FROM minhasTarefas ORDER BY id DESC", null);
            cursor.moveToFirst();

            int indiceColunaId = cursor.getColumnIndex("id");
            int indiceColunaTarefa = cursor.getColumnIndex("tarefa");

            itens = new ArrayList<String>();
            ids = new ArrayList<Integer>();

            itensAdapter = new ArrayAdapter<String>(getApplicationContext(),
                    android.R.layout.simple_list_item_2,
                    android.R.id.text1,
                    itens);
            lstLista.setAdapter(itensAdapter);

            while (cursor != null) {
                Log.i("LogX", "ID: " + cursor.getString(indiceColunaId)
                        + " Tarefa: " + cursor.getString(indiceColunaTarefa));
                ids.add(cursor.getInt(indiceColunaId));
                itens.add(cursor.getString(indiceColunaTarefa));
                cursor.moveToNext();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void adicionarNovaTarefa(String novaTarefa) {
        try {
            if (!novaTarefa.equals("")) {
                bd.execSQL("INSERT INTO minhasTarefas(tarefa)"
                        + " VALUES ('" + novaTarefa + "')");
                txtTexto.setText("");
                Toast.makeText(this, "Tarefa inserida!", Toast.LENGTH_LONG).show();
                carregaTarefas();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void apagarTarefa(Integer id) {
        try {
            bd.execSQL("DELETE FROM minhasTarefas WHERE id = " + id);
            carregaTarefas();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void alertaApagar(Integer id) {
        String tarefaSelecionada = itens.get(id);
        final Integer numeroId = id;

        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Aviso!")
                .setMessage("Deseja apagar a tarefa: " + tarefaSelecionada + " ?")
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        apagarTarefa(ids.get(numeroId));
                    }
                })
                .setNegativeButton("Não", null).show();
    }
}
